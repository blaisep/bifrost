for name in $(kubectl get namespace --output=jsonpath={.items..metadata.name})
do 
if [[ $name == ci-* ]] ; 
then
    releases=$(helm ls -q -n $name | wc -l);
    if [[ $releases -gt 0 ]] ;
	    then  helm uninstall $(helm ls -q -n $name) -n $name ; 
	fi
    kubectl delete all --all -n $name && kubectl delete namespace $name;
else echo "skipping namespace $name" ;
fi
done
